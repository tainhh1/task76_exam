'use strict';
$(document).ready(() => {
  let gDepartmentId = 0;
  let department = {
    newDepartment: {
      departmentCode: '',
      departmentName: '',
      business: '',
      introduction: '',
    },
    departmentDb: '',
    // khai báo table
    tableDepartment: $('#table-department').DataTable({
      order: false,
      columns: [
        { data: 'id' },
        { data: 'departmentCode' },
        { data: 'departmentName' },
        { data: 'business' },
        { data: 'introduction' },
        { data: 'Action' },
      ],
      columnDefs: [
        {
          targets: -1,
          defaultContent: `<i class="fas fa-edit text-primary"></i> |  <i class="fas fa-trash-alt text-danger"></i>`,
        },
      ],
    }),
    // get Department
    getDepartment() {
      $.ajax({
        url: `http://localhost:8080/api/departments`,
        method: 'get',
        async: false,
        dataType: 'json',
        success: (responseDepartment) =>
          (this.departmentDb = responseDepartment),
        error: (err) => alert(err.responseText),
      });
    },
    // load Department to table
    loadDepartmentToTable(paramDepartment) {
      this.tableDepartment.clear();
      this.tableDepartment.rows.add(paramDepartment);
      this.tableDepartment.draw();
    },
    // check department code
    checkDepartmentCode(paramDepartmentCode) {
      let vResult = this.departmentDb.some(
        (department) => paramDepartmentCode == department.departmentCode,
      );
      return vResult;
    },
    onCreateDepartmentClick() {
      gDepartmentId = 0;
      $('#modal-save').modal('show');
      resetInput();
    },
    onEditDepartmentClick() {
      $('#modal-save').modal('show');
      let vSelectedRow = $(this).parents('tr');
      let vSelectedData = department.tableDepartment.row(vSelectedRow).data();
      gDepartmentId = vSelectedData.id;
      $.get(
        `http://localhost:8080/api/departments/${gDepartmentId}`,
        loadDepartmentToInput,
      );
    },
    onSaveDepartmentClick() {
      this.newDepartment = {
        departmentCode: $('#input-department-code').val(),
        departmentName: $('#input-department-name').val(),
        business: $('#input-department-business').val(),
        introduction: $('#input-department-introduction').val(),
      };
      if (validateInfo(this.newDepartment)) {
        department.saveDepartment(this.newDepartment);
      }
    },
    saveDepartment(paramDepartment) {
      if (gDepartmentId === 0) {
        $.ajax({
          url: `http://localhost:8080/api/departments`,
          method: 'POST',
          data: JSON.stringify(paramDepartment),
          contentType: `application/json; charset=UTF-8`,
          success: this.loadToWebSite,
          error: (err) => alert(err.responseText),
        });
      } else {
        $.ajax({
          url: `http://localhost:8080/api/departments/${gDepartmentId}`,
          method: 'PUT',
          data: JSON.stringify(paramDepartment),
          contentType: `application/json; charset=UTF-8`,
          success: this.loadToWebSite,
          error: (err) => alert(err.responseText),
        });
      }
    },
    loadToWebSite() {
      alert('successfully update Department');
      department.getDepartment();
      department.loadDepartmentToTable(department.departmentDb);
      gDepartmentId = 0;
      $('#modal-save').modal('hide');
      resetInput();
    },
    onDeleteDepartmentByIdClick() {
      $('#modal-delete').modal('show');
      let vSelectedRow = $(this).parents('tr');
      let vSelectedData = department.tableDepartment.row(vSelectedRow).data();
      gDepartmentId = vSelectedData.id;
    },
    onDeleteAllDepartmentClick() {
      $('#modal-delete').modal('show');
      gDepartmentId = 0;
    },
    onConfirmDeleteClick() {
      if (gDepartmentId === 0) {
        $.ajax({
          url: `http://localhost:8080/api/departments`,
          method: 'delete',
          success: () => {
            $('#modal-delete').modal('hide');
            alert(`successfully delete all Department`);
            department.getDepartment();
            department.loadDepartmentToTable(department.departmentDb);
          },
          error: (err) => alert(err.responseText),
        });
      } else {
        $.ajax({
          url: `http://localhost:8080/api/departments/${gDepartmentId}`,
          method: 'delete',
          success: () => {
            $('#modal-delete').modal('hide');
            alert(`successfully delete Department with id: ${gDepartmentId}`);
            department.getDepartment();
            department.loadDepartmentToTable(department.departmentDb);
          },
          error: (err) => alert(err.responseText),
        });
      }
    },
  };
  // get Department
  department.getDepartment();
  // load department
  department.loadDepartmentToTable(department.departmentDb);
  // add click tạo mới department
  $('#btn-create-department').click(department.onCreateDepartmentClick);
  // add click icon edit department
  $('#table-department').on(
    'click',
    '.fa-edit',
    department.onEditDepartmentClick,
  );
  // add click save Department
  $('#btn-save-info').click(department.onSaveDepartmentClick);
  // add click icon edit department
  $('#table-department').on(
    'click',
    '.fa-trash-alt',
    department.onDeleteDepartmentByIdClick,
  );
  // add click delete All department
  $('#btn-delete-all-department').click(department.onDeleteAllDepartmentClick);
  // add click confirm delete
  $('#btn-confirm-delete').click(department.onConfirmDeleteClick);

  // validate info
  function validateInfo(paramDepartment) {
    let vResult = true;
    try {
      if (paramDepartment.departmentCode == '') {
        vResult = false;
        throw '100. Mã phòng ban không được để trống';
      }
      if (
        department.checkDepartmentCode(paramDepartment.departmentCode) &&
        gDepartmentId == 0
      ) {
        vResult = false;
        throw '101. Mã phòng ban đã tồn tại';
      }
      if (paramDepartment.departmentName == '') {
        vResult = false;
        throw '200. Tên phòng ban không được để trống ';
      }
      if (paramDepartment.business == '') {
        vResult = false;
        throw '300. Chức năng phòng ban không được để trống ';
      }
      if (paramDepartment.introduction == '') {
        vResult = false;
        throw '400. Thông tin phòng ban không được để trống ';
      }
    } catch (error) {
      alert(error);
    }
    return vResult;
  }

  // load department to input
  function loadDepartmentToInput(paramDepartment) {
    'use strict';
    $('#input-department-code').val(paramDepartment.departmentCode);
    $('#input-department-name').val(paramDepartment.departmentName);
    $('#input-department-business').val(paramDepartment.business);
    $('#input-department-introduction').val(paramDepartment.introduction);
  }

  // reset input
  function resetInput() {
    $('#input-department-code').val('');
    $('#input-department-name').val('');
    $('#input-department-business').val('');
    $('#input-department-introduction').val('');
  }
});
