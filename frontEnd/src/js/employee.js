$(document).ready(() => {
  $.get('http://localhost:8080/api/departments', loadDepartmentToSelect);
  function loadDepartmentToSelect(paramDepartment) {
    let selectElement = $('.select-department').select2({
      theme: 'bootstrap4',
    });
    paramDepartment.forEach((department) => {
      $('<option>', {
        text: department.departmentName,
        value: department.id,
      }).appendTo(selectElement);
    });
  }

  //Date picker
  $('#reservationdate').datetimepicker({
    format: 'DD/MM/YYYY',
  });

  let employeeTable = $('#table-employee').DataTable({
    columns: [
      { data: 'id' },
      { data: 'employeeCode' },
      { data: 'employeeName' },
      { data: 'gender' },
      { data: 'position' },
      { data: 'address' },
      { data: 'phoneNumber' },
      { data: 'birthDay' },
      { data: 'Action' },
    ],
    columnDefs: [
      {
        targets: -1,
        defaultContent: `<i class="fas fa-user-edit text-primary"></i> | <i class="fas fa-user-minus text-danger"></i>`,
      },
    ],
  });

  let gDepartmentIdSelect = 0;
  let gDepartmentId = 0;
  let gEmployeeId = 0;

  let employee = {
    employeeDb: '',
    checkEmployeeCode(paramEmployeeCode) {
      let vResult = this.employeeDb.some(
        (pEmployee) => paramEmployeeCode == pEmployee.employeeCode,
      );
      return vResult;
    },
    checkEmployeePhone(paramEmployeePhone) {
      let vResult = this.employeeDb.some(
        (pEmployee) => pEmployee.phoneNumber == paramEmployeePhone,
      );
      return vResult;
    },
    onSelectDepartmentChange(e) {
      gDepartmentIdSelect = e.target.value;
      if (gDepartmentIdSelect == 0) {
        employee.getEmployee(`http://localhost:8080/api/employees`);
      } else {
        employee.getEmployee(
          `http://localhost:8080/api/departments/${gDepartmentIdSelect}/employees`,
        );
      }
    },
    getEmployee(paramUrl) {
      $.ajax({
        url: paramUrl,
        method: 'get',
        dataType: 'json',
        async: false,
        success: loadEmployeeToTable,
        error: (err) => alert(err.responseText),
      });
    },
    onCreateNewEmployeeClick() {
      $('#modal-save').modal('show');
      $('#sub-select').prop('disabled', false);
      gEmployeeId = 0;
      resetInput();
    },
    onEditEmployeeClick() {
      $('#modal-save').modal('show');
      $('#sub-select').prop('disabled', true);
      let vSelectedRow = $(this).parents('tr');
      let vSelectedData = employeeTable.row(vSelectedRow).data();
      gEmployeeId = vSelectedData.id;
      $.get(
        `http://localhost:8080/api/employees/${gEmployeeId}`,
        loadEmployeeToInput,
      );
    },
    newEmployee: {
      employeeCode: '',
      employeeName: '',
      position: '',
      gender: '',
      birthDay: '',
      address: '',
      phoneNumber: '',
    },
    onSaveEmployeeClick() {
      this.newEmployee = {
        employeeCode: $('#input-employee-code').val(),
        employeeName: $('#input-employee-name').val(),
        position: $('#input-employee-position').val(),
        gender: $('#select-employee-gender').val(),
        birthDay: $('.input-employee-birthday').val(),
        address: $('#input-employee-address').val(),
        phoneNumber: $('#input-employee-phone').val(),
      };
      if (validateInfo(this.newEmployee)) {
        employee.saveEmployee(this.newEmployee);
      }
    },
    saveEmployee(paramEmployee) {
      if (gEmployeeId == 0) {
        $.ajax({
          url: `http://localhost:8080/api/departments/${gDepartmentId}/employees`,
          method: 'post',
          contentType: `application/json; charset=UTF-8`,
          data: JSON.stringify(paramEmployee),
          success: loadEmployeeToWebsite,
          error: (err) => alert(err.responseText),
        });
      } else {
        $.ajax({
          url: `http://localhost:8080/api/departments/employees/${gEmployeeId}`,
          method: 'put',
          contentType: `application/json; charset=UTF-8`,
          data: JSON.stringify(paramEmployee),
          success: loadEmployeeToWebsite,
          error: (err) => alert(err.responseText),
        });
      }
    },
    onDeleteAllEmployeeClick() {
      $('#modal-delete').modal('show');
      gEmployeeId = 0;
    },
    onDeleteEmployeeById() {
      $('#modal-delete').modal('show');
      let vSelectedRow = $(this).parents('tr');
      let vSelectedData = employeeTable.row(vSelectedRow).data();
      gEmployeeId = vSelectedData.id;
    },
    onConfirmDeleteEmployee() {
      if (gEmployeeId == 0) {
        $.ajax({
          url: `http://localhost:8080/api/departments/employees`,
          method: 'delete',
          success: () => {
            alert('All Employees were deleted');
            $('#modal-delete').modal('hide');
            gEmployeeId = 0;
            employee.getEmployee(`http://localhost:8080/api/employees`);
          },
        });
      } else {
        $.ajax({
          url: `http://localhost:8080/api/departments/employees/${gEmployeeId}`,
          method: 'delete',
          success: () => {
            alert(`Employee with id ${gEmployeeId} was successfully deleted`);
            $('#modal-delete').modal('hide');
            gEmployeeId = 0;
            employee.getEmployee(`http://localhost:8080/api/employees`);
          },
        });
      }
    },
  };
  employee.getEmployee(`http://localhost:8080/api/employees`);
  function loadEmployeeToWebsite() {
    alert('Successfully update Employee');
    $('#modal-save').modal('hide');
    resetInput();
    gEmployeeId = 0;
    employee.getEmployee(`http://localhost:8080/api/employees`);
  }

  // add on change cho main select
  $('#main-select').change(employee.onSelectDepartmentChange);
  // add on change cho sub select
  $('#sub-select').change((e) => (gDepartmentId = e.target.value));
  // add click cho nút tạo mới employee
  $('#btn-create-employee').click(employee.onCreateNewEmployeeClick);
  // add click cho icon edit employee
  $('#table-employee').on(
    'click',
    '.fa-user-edit',
    employee.onEditEmployeeClick,
  );
  // add click save info
  $('#btn-save-info').click(employee.onSaveEmployeeClick);
  // add delete All Employee
  $('#btn-delete-all-employee').click(employee.onDeleteAllEmployeeClick);
  // add click cho icon edit employee
  $('#table-employee').on(
    'click',
    '.fa-user-minus ',
    employee.onDeleteEmployeeById,
  );
  // add click confirm delete employee
  $('#btn-confirm-delete').click(employee.onConfirmDeleteEmployee);

  function loadEmployeeToInput(paramEmployee) {
    $('#input-employee-code').val(paramEmployee.employeeCode);
    $('#input-employee-name').val(paramEmployee.employeeName);
    $('#select-employee-gender').val(paramEmployee.gender);
    $('#input-employee-position').val(paramEmployee.position);
    $('#input-employee-address').val(paramEmployee.address);
    $('#input-employee-phone').val(paramEmployee.phoneNumber);
    $('.input-employee-birthday').val(paramEmployee.birthDay);
  }

  function validateInfo(paramEmployee) {
    let vResult = true;
    try {
      if (paramEmployee.employeeCode == '') {
        vResult = false;
        throw '100. Mã nhân viên không được để trống';
      }
      if (
        employee.checkEmployeeCode(paramEmployee.employeeCode) &&
        gEmployeeId == 0
      ) {
        vResult = false;
        throw '101. Mã nhân viên bị trùng';
      }
      if (paramEmployee.employeeName == '') {
        vResult = false;
        throw '200. Tên nhân viên không được để trống';
      }
      if (paramEmployee.position == '') {
        vResult = false;
        throw '300. Chức vụ nhân viên không được để trống';
      }
      if (paramEmployee.gender == 0) {
        vResult = false;
        throw '400. Giới tính không được để trống';
      }
      if (paramEmployee.birthDay == '') {
        vResult = false;
        throw '500. Ngày tháng năm sinh không được để trống';
      }
      if (paramEmployee.address == '') {
        vResult = false;
        throw '600. Địa chỉ không được để trống';
      }
      if (paramEmployee.phoneNumber.length < 10) {
        vResult = false;
        throw '700. Số điện thoại không được để trống';
      }
      if (
        employee.checkEmployeePhone(paramEmployee.phoneNumber) &&
        gEmployeeId == 0
      ) {
        vResult = false;
        throw '701. Đã có số điện thoại nhân viên';
      }
      if (gEmployeeId == 0) {
        if (gDepartmentId == 0) {
          vResult = false;
          throw '800. Bạn chưa chọn phòng ban cho nhân viên';
        }
      }
    } catch (error) {
      alert(error);
    }
    return vResult;
  }

  function resetInput() {
    $('#input-employee-code').val('');
    $('#input-employee-name').val('');
    $('#select-employee-gender').val(0);
    $('#input-employee-position').val('');
    $('#input-employee-address').val('');
    $('#input-employee-phone').val('');
    $('.input-employee-birthday').val('');
    $('#sub-select').val(0);
  }

  function loadEmployeeToTable(paramEmployee) {
    if (gDepartmentIdSelect == 0) {
      employee.employeeDb = paramEmployee;
    }
    employeeTable.clear();
    employeeTable.rows.add(paramEmployee);
    employeeTable.draw();
  }
});
