package com.devcamp.exam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.exam.model.Department;

public interface DepartmentRepo extends JpaRepository<Department, Long> {
}
