package com.devcamp.exam.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.exam.model.Department;
import com.devcamp.exam.model.Employee;
import com.devcamp.exam.repository.DepartmentRepo;
import com.devcamp.exam.repository.EmployeeRepo;

@CrossOrigin
@RestController
public class EmployeeController {
	@Autowired
	EmployeeRepo employeeRepo;
	@Autowired
	DepartmentRepo departmentRepo;

	@GetMapping("/employees")
	public ResponseEntity<Object> getAllEmployee() {
		try {
			return new ResponseEntity<>(employeeRepo.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/employees/{employeeId}")
	public ResponseEntity<Object> getEmployeeById(@PathVariable Long employeeId) {
		try {
			Optional<Employee> employeeFound = employeeRepo.findById(employeeId);
			if (employeeFound.isPresent()) {
				return new ResponseEntity<>(employeeFound.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>("can't find Employee with specified id: " + employeeId,
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/departments/{departmentId}/employees")
	public ResponseEntity<Object> getEmployeeByDepartmentId(@PathVariable Long departmentId) {
		try {
			Optional<Department> departMentFound = departmentRepo.findById(departmentId);
			if (departMentFound.isPresent()) {
				List<Employee> employeeFound = employeeRepo.findByDepartmentId(departmentId);
				return new ResponseEntity<>(employeeFound, HttpStatus.OK);
			} else {
				return new ResponseEntity<>("can't find Department with specified id: " + departmentId,
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/departments/{departmentId}/employees")
	public ResponseEntity<Object> createEmployee(@Valid @RequestBody Employee inEmployee,
			@PathVariable Long departmentId) {
		try {
			Optional<Department> departmentFound = departmentRepo.findById(departmentId);
			if (departmentFound.isPresent()) {
				inEmployee.setDepartment(departmentFound.get());
				return new ResponseEntity<>(employeeRepo.save(inEmployee), HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>("can't find Department with specified id: " + departmentId,
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/departments/employees/{employeeId}")
	public ResponseEntity<Object> updateEmployee(@Valid @RequestBody Employee inEmployee,
			@PathVariable Long employeeId) {
		try {
			Optional<Employee> employeeFound = employeeRepo.findById(employeeId);
			if (employeeFound.isPresent()) {
				Employee updateEmployee = employeeFound.get();
				updateEmployee.setId(employeeId);
				updateEmployee.setAddress(inEmployee.getAddress());
				updateEmployee.setBirthDay(inEmployee.getBirthDay());
				updateEmployee.setEmployeeCode(inEmployee.getEmployeeCode());
				updateEmployee.setEmployeeName(inEmployee.getEmployeeName());
				updateEmployee.setGender(inEmployee.getGender());
				updateEmployee.setPhoneNumber(inEmployee.getPhoneNumber());
				updateEmployee.setPosition(inEmployee.getPosition());
				return new ResponseEntity<>(employeeRepo.save(updateEmployee), HttpStatus.OK);
			} else {
				return new ResponseEntity<>("can't find Employee with specified id: " + employeeId,
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/departments/employees/{employeeId}")
	public ResponseEntity<Object> deleteEmployeeById(@PathVariable Long employeeId) {
		try {
			Optional<Employee> employeeFound = employeeRepo.findById(employeeId);
			if (employeeFound.isPresent()) {
				employeeRepo.deleteById(employeeId);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>("can't find Employee with specified id: " + employeeId,
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/departments/employees")
	public ResponseEntity<Object> deleteAllEmployee() {
		try {
			employeeRepo.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
