package com.devcamp.exam.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.exam.model.Department;
import com.devcamp.exam.repository.DepartmentRepo;

@CrossOrigin
@RestController
public class DepartmentController {
	@Autowired
	DepartmentRepo departmentRepo;

	@GetMapping("/departments")
	public ResponseEntity<Object> getAllDepartment() {
		try {
			return new ResponseEntity<>(departmentRepo.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/departments/{departmentId}")
	public ResponseEntity<Object> getDepartmentById(@PathVariable Long departmentId) {
		try {
			Optional<Department> departmentFound = departmentRepo.findById(departmentId);
			if (departmentFound.isPresent()) {
				return new ResponseEntity<>(departmentFound.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>("can't find specified department with id: " + departmentId,
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/departments")
	public ResponseEntity<Object> createDepartment(@Valid @RequestBody Department inDepartment) {
		try {
			return new ResponseEntity<>(departmentRepo.save(inDepartment), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/departments/{departmentId}")
	public ResponseEntity<Object> updateDepartment(@Valid @RequestBody Department inDepartment,
			@PathVariable Long departmentId) {
		try {
			Optional<Department> departmentFound = departmentRepo.findById(departmentId);
			if (departmentFound.isPresent()) {
				Department updateDepartment = departmentFound.get();
				updateDepartment.setId(departmentId);
				updateDepartment.setBusiness(inDepartment.getBusiness());
				updateDepartment.setDepartmentCode(inDepartment.getDepartmentCode());
				updateDepartment.setDepartmentName(inDepartment.getDepartmentName());
				updateDepartment.setIntroduction(inDepartment.getIntroduction());
				return new ResponseEntity<>(departmentRepo.save(updateDepartment), HttpStatus.OK);
			} else {
				return new ResponseEntity<>("can't find specified department with id: " + departmentId,
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/departments/{departmentId}")
	public ResponseEntity<Object> deleteDepartmentById(@PathVariable Long departmentId) {
		try {
			departmentRepo.deleteById(departmentId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/departments")
	public ResponseEntity<Object> deleteDepartmentById() {
		try {
			departmentRepo.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
