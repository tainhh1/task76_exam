package com.devcamp.exam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.exam.model.Employee;

public interface EmployeeRepo extends JpaRepository<Employee, Long> {
	List<Employee> findByDepartmentId(Long departmentId);
}
