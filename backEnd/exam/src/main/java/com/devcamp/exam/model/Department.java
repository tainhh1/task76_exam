package com.devcamp.exam.model;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "departments")
public class Department {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "departmentCode", unique = true, nullable = false)
	@NotEmpty(message = "Department Code can't be empty")
	private String departmentCode;

	@Column(name = "departmentName", nullable = false)
	@NotEmpty(message = "Department Name can't be empty")
	private String departmentName;

	@Column(name = "business", nullable = false)
	@NotEmpty(message = "Business can't be empty")
	private String business;

	@Column(name = "introduction", nullable = false)
	@NotEmpty(message = "Introduction of Department can't be empty")
	private String introduction;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "department")
	private List<Employee> employees;

	public Department() {
		super();
	}

	public Department(Long id, @NotEmpty(message = "Department Code can't be empty") String departmentCode,
			@NotEmpty(message = "Department Name can't be empty") String departmentName,
			@NotEmpty(message = "Business can't be empty") String business,
			@NotEmpty(message = "Introduction of Department can't be empty") String introduction,
			List<Employee> employees) {
		super();
		this.id = id;
		this.departmentCode = departmentCode;
		this.departmentName = departmentName;
		this.business = business;
		this.introduction = introduction;
		this.employees = employees;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDepartmentCode() {
		return departmentCode;
	}

	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getBusiness() {
		return business;
	}

	public void setBusiness(String business) {
		this.business = business;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
}
