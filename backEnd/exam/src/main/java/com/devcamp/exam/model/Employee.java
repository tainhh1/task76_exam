package com.devcamp.exam.model;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "employees")
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "employeeCode", nullable = false, unique = true)
	@NotEmpty(message = "Employee Code can't be empty")
	private String employeeCode;

	@Column(name = "employeeName", nullable = false)
	@NotEmpty(message = "Employee Code can't be empty")
	private String employeeName;

	@Column(name = "position", nullable = false)
	@NotEmpty(message = "position can't be empty")
	private String position;

	@Column(name = "gender", nullable = false)
	@NotEmpty(message = "gender can't be empty")
	private String gender;

	@Column(name = "birthDay", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd/MM/yyyy", timezone="GMT+7")
	private Date birthDay;

	@Column(name = "address", nullable = false)
	@NotEmpty(message = "address can't be empty")
	private String address;

	@Column(name = "phoneNumber", nullable = false, unique = true)
	@Size(min = 10, message = "Phone must be right content min = 10")
	@NotEmpty(message = "Phone can't be empty")
	private String phoneNumber;

	@ManyToOne(fetch = FetchType.LAZY, cascade = {
			CascadeType.PERSIST,
			CascadeType.MERGE
	})
	@JoinColumn(name = "departmentId", nullable = false)
	@JsonIgnore
	private Department department;

	public Employee() {
		super();
	}

	public Employee(Long id, @NotEmpty(message = "Employee Code can't be empty") String employeeCode,
			@NotEmpty(message = "Employee Code can't be empty") String employeeName,
			@NotEmpty(message = "position can't be empty") String position,
			@NotEmpty(message = "gender can't be empty") String gender, Date birthDay,
			@NotEmpty(message = "address can't be empty") String address,
			@NotEmpty(message = "Phone can't be empty") String phoneNumber, Department department) {
		super();
		this.id = id;
		this.employeeCode = employeeCode;
		this.employeeName = employeeName;
		this.position = position;
		this.gender = gender;
		this.birthDay = birthDay;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.department = department;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}
}
